import os
import random
import re
import shutil
import time

from logic.dal import DAL
from logic.generators.tools.pom_generator import PomGenerator
from logic.structures.artifactdetails import ArtifactDetails
from logic.zipper import zip_folder


class ModuleGenerator:
    def __init__(self, params: dict, working_folder: str, dal: DAL):
        self.params = params
        self.working_folder = working_folder
        os.makedirs(os.path.join(working_folder, 'OSFGenerator'), exist_ok=True)
        self.temp_folder = os.path.join(working_folder, 'OSFGenerator', str(round(time.time())) + '_' + str(random.randint(0, 1000)))
        self.outcome_folder = os.path.join(self.temp_folder, self.params['name'].strip().lower().replace(' ', '_'))
        self.dependency_regex = re.compile('dependency_(\\d*)')
        self.dal = dal

    def generate(self) -> bytes:
        """
        Generates a module out of provided params and returns the bytes of a zip containing the outcome folder
        """

        self.__build_maven_structures()
        self.__generate_pom_xml_file()

        zipped_folder = zip_folder(folder_path=self.outcome_folder)
        shutil.rmtree(self.temp_folder)
        return zipped_folder

    def __build_maven_structures(self):
        module_name = self.params['name']
        maven_paths = [
                ['src', 'main'],
                ['src', 'main', 'java'],
                ['src', 'main', 'java', module_name],
                ['src', 'main', 'resources'],
                ['src', 'test'],
                ['src', 'test', 'java'],
                ['src', 'test', 'java', module_name],
        ]
        for path in maven_paths:
            os.makedirs(os.path.join(self.outcome_folder, *path), exist_ok=True)

    def __generate_pom_xml_file(self):
        dependency_ids = []
        for key in self.params.keys():
            result = re.search(self.dependency_regex, key)
            if not result:
                continue
            if len(result.groups()) == 1:
                dependency_ids.append(result.group(1))

        dependencies = self.dal.fetch_module_artifact_details(module_ids=dependency_ids)
        pom_generator = PomGenerator(
                jar_details=ArtifactDetails(self.params['group'],
                                            self.params['artifact'],
                                            self.params['version']),
                dependencies=dependencies
        )
        pom_content = pom_generator.generate()
        with open(os.path.join(self.outcome_folder, 'pom.xml'), 'w') as pom_file:
            pom_file.write(pom_content)
