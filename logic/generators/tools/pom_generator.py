from typing import List

from lxml import etree

from logic.structures.artifactdetails import ArtifactDetails


class PomGenerator:
    def __init__(self, jar_details: ArtifactDetails, dependencies: List[ArtifactDetails]):
        super().__init__()
        self.jar_details = jar_details
        self.dependencies = dependencies

    def generate(self) -> str:
        namespace_map = {None: 'http://maven.apache.org/POM/4.0.0', 'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
        attr_qname = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        project = etree.Element("project", {attr_qname: 'http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd'}, nsmap=namespace_map)
        group_id = etree.SubElement(project, 'groupId')
        group_id.text = self.jar_details.groupId
        artifact_id = etree.SubElement(project, 'artifactId')
        artifact_id.text = self.jar_details.artifactId
        version = etree.SubElement(project, 'version')
        version.text = self.jar_details.version
        properties_element = etree.SubElement(project, 'properties')
        source_encoding = etree.SubElement(properties_element, 'project.build.sourceEncoding')
        source_encoding.text = 'UTF-8'
        reporting_encoding = etree.SubElement(properties_element, 'project.reporting.outputEncoding')
        reporting_encoding.text = 'UTF-8'
        source_compiler = etree.SubElement(properties_element, 'maven.compiler.source')
        source_compiler.text = '11'
        target_compiler = etree.SubElement(properties_element, 'maven.compiler.target')
        target_compiler.text = '11'
        dependencies_element = etree.SubElement(project, "dependencies")
        for dependency in self.dependencies:
            dependency_element = etree.SubElement(dependencies_element, 'dependency')
            group_id = etree.SubElement(dependency_element, 'groupId')
            group_id.text = dependency.groupId
            artifact_id = etree.SubElement(dependency_element, 'artifactId')
            artifact_id.text = dependency.artifactId
            version = etree.SubElement(dependency_element, 'version')
            version.text = dependency.version
        return bytes.decode(etree.tostring(project, xml_declaration=True, pretty_print=True, encoding='UTF8'))
