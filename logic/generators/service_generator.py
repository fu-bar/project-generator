import os
import random
import time


class ServiceGenerator:
    def __init__(self, params: dict, working_folder: str):
        self.params = params
        self.folder = working_folder
        try:
            os.makedirs(os.path.join(working_folder, 'OSFGenerator'))
        except FileExistsError:
            pass
        self.temp_folder = os.path.join(working_folder, 'OSFGenerator', str(round(time.time())), '_', str(random.randint(0, 1000)))

    def generate(self) -> bytes:
        """
        Generates a service out of provided params and returns the folder with results
        """
        return bytes('')
