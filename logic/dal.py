import sqlite3
from typing import List

from logic.structures.artifactdetails import ArtifactDetails
from logic.structures.osf_module_info import OSFModuleInfo


class DAL:
    def __init__(self, db_path: str):
        self.db_path = db_path

    @staticmethod
    def __dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def __run_query(self, query: str):
        sqlite_connection = None
        try:
            sqlite_connection = sqlite3.connect(self.db_path)
            sqlite_connection.row_factory = DAL.__dict_factory
            cursor = sqlite_connection.cursor()

            cursor.execute(query)
            module_records = cursor.fetchall()
            cursor.close()
            sqlite_connection.close()
            return module_records
        except sqlite3.Error as error:
            print("Error while connecting to sqlite", error)
        finally:
            if sqlite_connection:
                sqlite_connection.close()

    @staticmethod
    def __to_osf_module_infos(records) -> List[OSFModuleInfo]:
        module_infos = []
        for record in records:
            module_infos.append(
                    OSFModuleInfo(
                            record['moduleId'],
                            record['displayName'],
                            record['description'],
                            record['groupId'],
                            record['artifactId'],
                            record['version'],
                    )
            )
        return module_infos

    @staticmethod
    def __to_artifact_details(records) -> List[ArtifactDetails]:
        artifact_details = []
        for record in records:
            artifact_details.append(
                    ArtifactDetails(
                            group_id=record['groupId'],
                            artifact_id=record['artifactId'],
                            version=record['version'],
                    )
            )
        return artifact_details

    def fetch_modules(self) -> List[OSFModuleInfo]:
        module_records = self.__run_query(query="select * from modules;")
        return DAL.__to_osf_module_infos(records=module_records)

    def fetch_module_artifact_details(self, module_ids: List[str]) -> List[ArtifactDetails]:
        module_records = self.__run_query(query="select * from modules WHERE moduleId IN ({});".format(','.join(module_ids)))
        return DAL.__to_artifact_details(records=module_records)
