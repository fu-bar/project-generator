import io
import os
import zipfile


# def zip_folder(folder_path: str) -> bytes:
#     zip_buffer = io.BytesIO()
#     with zipfile.ZipFile(zip_buffer, "w", zipfile.ZIP_BZIP2, False) as zip_file:
#         for root, _, filenames in os.walk(folder_path):
#             for filename in filenames:
#                 filename = os.path.join(root, filename)
#                 filename = os.path.normpath(filename)
#                 zip_file.write(filename=filename, arcname=filename)
#     zip_buffer.seek(0)
#     return zip_buffer.read()


def zip_folder(folder_path: str):
    zip_buffer = io.BytesIO()
    '''Zip up a directory and preserve symlinks and empty directories'''
    zip_out = zipfile.ZipFile(zip_buffer, 'w', compression=zipfile.ZIP_BZIP2)

    root_length = len(os.path.dirname(folder_path))

    def _archive_directory(parent_directory):
        contents = os.listdir(parent_directory)
        # store empty directories
        if not contents:
            archive_root = parent_directory[root_length:].replace('\\', '/').lstrip('/')
            zip_info = zipfile.ZipInfo(archive_root + '/')
            zip_out.writestr(zip_info, '')
        for item in contents:
            full_path = os.path.join(parent_directory, item)
            if os.path.isdir(full_path) and not os.path.islink(full_path):
                _archive_directory(full_path)
            else:
                archive_root = full_path[root_length:].replace('\\', '/').lstrip('/')
                if os.path.islink(full_path):
                    zip_info = zipfile.ZipInfo(archive_root)
                    zip_info.create_system = 3
                    zip_info.external_attr = 2716663808
                    zip_out.writestr(zip_info, os.readlink(full_path))
                else:
                    zip_out.write(full_path, archive_root, zipfile.ZIP_DEFLATED)

    _archive_directory(folder_path)

    zip_out.close()
    zip_buffer.seek(0)
    return zip_buffer.read()
