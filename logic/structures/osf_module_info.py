class OSFModuleInfo:
    def __init__(self, module_id: int, display_name: str, description: str, group_id: str, artifact_id: str, version: str):
        self.module_id = module_id
        self.display_name = display_name
        self.description = description
        self.group_id = group_id
        self.artifact_id = artifact_id
        self.version = version
