class ArtifactDetails:
    def __init__(self, group_id: str, artifact_id: str, version: str):
        self.groupId = group_id
        self.artifactId = artifact_id
        self.version = version
