class Metadata:
    def __init__(self, label: str, placeholder: str, pattern_regex: str) -> None:
        self.label = label
        self.placeholder = placeholder
        self.pattern_regex = pattern_regex
