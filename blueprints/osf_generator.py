from flask import abort
from flask import Blueprint
from flask import make_response
from flask import render_template
from flask import request

from blueprints.metadata import Metadata
from logic.dal import DAL
from logic.generators.module_generator import ModuleGenerator
from logic.generators.service_generator import ServiceGenerator

osf_generator = Blueprint('service_generator', __name__, template_folder='templates')

metadata = [
        Metadata(label='Group', placeholder='com.example', pattern_regex='[a-zA-Z\\.]+'),
        Metadata(label='Artifact', placeholder='demo', pattern_regex='[a-zA-z]+'),
        Metadata(label='Name', placeholder='name', pattern_regex='[a-z]+'),
        Metadata(label='Description', placeholder='Demo OSF service', pattern_regex='.*'),
        Metadata(label='Package', placeholder='com.example.demo', pattern_regex='[a-zA-Z\\.]+'),
        Metadata(label='Version', placeholder='1.0.0', pattern_regex='[0-9\\.]+'),
]

dal = DAL(db_path=r'c:\workspace\fubar\db\generator.db')


@osf_generator.route('/osf-generator', methods=['GET'])
def show_user_form():
    module_infos = dal.fetch_modules()
    response = make_response(render_template('index.html', metadata=metadata, module_infos=module_infos))
    return response


@osf_generator.route('/osf-generator', methods=['POST'])
def generate():
    params = request.form.to_dict()
    action = params['action']

    zipped_outcome = None
    if action == 'generateService':
        zipped_outcome = ServiceGenerator(params=params, working_folder=r'c:\temp').generate()
    elif action == 'generateModule':
        zipped_outcome = ModuleGenerator(params=params, working_folder=r'c:\temp', dal=dal).generate()
    else:
        abort(500, 'Unexpected Request')

    file_name = params['name'].strip().lower().replace(' ', '_') + '.zip'

    response = make_response(zipped_outcome)
    response.headers.set('Content-Type', 'zip')
    response.headers.set('Content-Disposition', 'attachment', filename=file_name)
    return response
