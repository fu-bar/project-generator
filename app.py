from flask import Flask

from blueprints.osf_generator import osf_generator

app = Flask(__name__)
app.secret_key = "secret key"
app.register_blueprint(osf_generator)

if __name__ == '__main__':
    app.run()
